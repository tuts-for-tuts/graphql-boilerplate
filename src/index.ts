import "reflect-metadata";
import { importSchema } from "graphql-import";

import { resolvers } from "./resolvers";
import { GraphQLServer } from 'graphql-yoga'
import { createConnection } from "typeorm";

// ... or using `require()`
// const { GraphQLServer } = require('graphql-yoga')

const typeDefs = importSchema(`${__dirname}/schema.graphql`)


const server = new GraphQLServer({ typeDefs, resolvers })
createConnection().then(() => {
    server.start(() => console.log('Server is running on localhost:4000'))
})



