import {Entity, PrimaryColumn, BaseEntity, Column, BeforeInsert} from "typeorm";
import * as uuidV4 from 'uuid/v4';

@Entity("_user")
export class User extends BaseEntity {

    @PrimaryColumn("uuid")
    id: string;

    @Column("varchar", { length: 255})
    email: string;

    @Column("text")
    password: string;

    @BeforeInsert()
    addId() {
        this.id = uuidV4();
    }
}
