import { ResolverMap } from "./types/graphql-utils";
import * as bcrypt from "bcryptjs";
import { User } from "./entity/User";

export const resolvers: ResolverMap = {
    Query: {
      hello: (_: any, { name }: GQL.IHelloOnQueryArguments) => `ByeBye ${name || 'World'}`,
    },

    Mutation: { 
        register: async (_, { email, password}: GQL.IRegisterOnMutationArguments ) => {
            
            const hashedPswd = await bcrypt.hash(password, 10);
            const user = User.create({
                email,
                password: hashedPswd
            });
            await user.save();

            return true;
        }
    }
  };